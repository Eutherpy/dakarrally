﻿using DakarRally.Models;
using DakarRally.Models.API;
using DakarRally.Models.Core;
using DakarRally.Models.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DakarRally.Controllers
{
    /// <summary>
    /// Vehicle controller
    /// </summary>
    [Route("api/[controller]")]
    public class VehicleController : ApiController
    {
        /// <summary>
        /// Update vehicle info. Only a vehicle participating in a pending race can be updated.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("update-info")]
        public IHttpActionResult UpdateVehicleInfo([FromBody]VehicleApi data)
        {
            Guid raceId = RaceManager.GetRaceForVehicle(data.VehicleId);
            if (RaceManager.GetRaceStatus(raceId) != RaceStatus.Pending)
                return BadRequest("Only a vehicle participating in a pending race can be updated.");
            try
            {
                VehicleManager.UpdateVehicleInfo(raceId, data.VehicleId,
                    new Vehicle((VehicleType)Enum.Parse(typeof(VehicleType), data.VehicleType), data.TeamName, data.VehicleModel, data.ManufacturingDate));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Ok();
        }

        /// <summary>
        /// Find vehicle(s)
        /// </summary>
        /// <param name="team"></param>
        /// <param name="model"></param>
        /// <param name="date"></param>
        /// <param name="distance"></param>
        /// <returns></returns>
        [HttpGet]
        //[Route("team={team}&model={model}&date={date}&distance={distance}")]
        [Route("find-vehicle")]
        public IHttpActionResult FindVehicle([FromUri]string team = "", [FromUri]string model = "", [FromUri]DateTime? date = null, [FromUri]double distance = 0.0)
        {
            List<Guid> listOfIds = StatsManager.Stats.Select(x => x.VehicleId).ToList(); // All vehicles
            List<Vehicle> list = new List<Vehicle>();
            foreach (var id in listOfIds)
                list.Add(RaceManager.FindVehicle(id));

            if (team != "")
                list = list.Where(x => x.TeamName == team).ToList();

            if (model != "")
                list = list.Where(x => x.VehicleModel == model).ToList();

            if (date != null)
                list = list.Where(x => x.ManufacturingDate == date).ToList();

            if (distance > 0)
            {
                foreach (var x in list)
                {
                    var stats = StatsManager.Stats.FirstOrDefault(y => y.VehicleId == x.VehicleId);
                    if (stats.Distance < distance)
                        list.Remove(x);
                }
            }

            return Ok(list);
        }
    }
}
