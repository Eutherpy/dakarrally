﻿using DakarRally.Models;
using DakarRally.Models.API;
using DakarRally.Models.Core;
using DakarRally.Models.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DakarRally.Controllers
{
    /// <summary>
    /// Race statistics controller
    /// </summary>
    [Route("api/[controller]")]
    public class StatsController : ApiController
    {
        /// <summary>
        /// Get the leaderboard for a race
        /// </summary>
        /// <param name="raceId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("leaderboard/raceid={raceId}")]
        public IHttpActionResult GetLeaderboard([FromUri]Guid raceId)
        {
            if (!RaceManager.CheckIfRaceExists(raceId) || RaceManager.GetRaceStatus(raceId) != RaceStatus.Finished)
                return BadRequest("Race is invald or unfinished.");
            List<VehicleStatsApi> list = new List<VehicleStatsApi>();
            try
            {
                foreach (VehicleStats stats in StatsManager.Stats)
                {
                    if (stats.RaceId == raceId)
                    {
                        Vehicle vehicle = RaceManager.GetVehicleFromRace(raceId, stats.VehicleId);
                        list.Add(new VehicleStatsApi(vehicle.VehicleType, vehicle.TeamName, vehicle.VehicleModel, stats.FinishTime));
                    }
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Ok(list.OrderBy(x => x.FinishTime));
        }

        /// <summary>
        /// Get the leaderboard for a race, for a specific vehicle type
        /// </summary>
        /// <param name="raceId"></param>
        /// <param name="vehicleType"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("leaderboard/raceid={raceId}&type={vehicleType}")]
        public IHttpActionResult GetLeaderboard([FromUri]Guid raceId, [FromUri]VehicleType vehicleType)
        {
            if (!RaceManager.CheckIfRaceExists(raceId) || RaceManager.GetRaceStatus(raceId) != RaceStatus.Finished)
                return BadRequest("Race is invald or unfinished.");
            List<VehicleStatsApi> list = new List<VehicleStatsApi>();
            try
            {
                foreach (VehicleStats stats in StatsManager.Stats)
                {
                    if (stats.RaceId == raceId)
                    {
                        Vehicle vehicle = RaceManager.GetVehicleFromRace(raceId, stats.VehicleId);
                        if (vehicle.VehicleType == vehicleType)
                            list.Add(new VehicleStatsApi(vehicle.VehicleType, vehicle.TeamName, vehicle.VehicleModel, stats.FinishTime));
                    }
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Ok(list.OrderBy(x => x.FinishTime));
        }

        /// <summary>
        /// Get the race statistics for a vehicle
        /// </summary>
        /// <param name="vehicleId"></param>
        /// <param name="raceId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("raceid={raceId}&vehicleid={vehicleId}")]
        public IHttpActionResult GetVehicleStats(Guid vehicleId, Guid raceId)
        {
            if (!RaceManager.CheckIfRaceExists(raceId) || RaceManager.GetRaceStatus(raceId) != RaceStatus.Finished)
                return BadRequest("Race is invald or unfinished.");

            VehicleStats statistics = new VehicleStats();
            foreach (VehicleStats stats in StatsManager.Stats)
            {
                if (stats.RaceId == raceId && stats.VehicleId == vehicleId)
                {
                    statistics = stats;
                }
                return Ok(statistics);
            }
            return BadRequest("Vehicle not found.");
        }

        /// <summary>
        /// Get detailed race status. 
        /// </summary>
        /// <param name="raceId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("raceid={raceId}")]
        public IHttpActionResult GetFullRaceStatus([FromUri]Guid raceId)
        {
            if (!RaceManager.CheckIfRaceExists(raceId))
                return BadRequest("Race identifier is invald.");
            try
            {
                RaceStatusApi fullStatus = new RaceStatusApi
                {
                    RaceStatus = RaceManager.GetRaceStatus(raceId).ToString(),
                    ParticipantsByType = RaceManager.GetNumberOfVehiclesByType(raceId),
                    NumberOfParticipantsFinished = RaceManager.GetNumberOfVehiclesFinished(raceId)
                };

                return Ok(fullStatus);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
