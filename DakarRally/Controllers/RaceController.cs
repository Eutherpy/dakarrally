﻿using DakarRally.Models;
using DakarRally.Models.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DakarRally.Controllers
{
    /// <summary>
    /// Race controller
    /// </summary>
    [Route("api/[controller]")]
    public class RaceController : ApiController
    {
        public const int YearLimit = 1999;

        /// <summary>
        /// Create a new race
        /// </summary>
        /// <param name="year"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("create")]
        public IHttpActionResult CreateRace([FromBody]int year)
        {
            if (year < YearLimit)
                return BadRequest("Invalid year.");
            Guid raceId;
            try
            {
                raceId = RaceManager.AddRace(year);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Ok(raceId);
        }

        /// <summary>
        /// Add a new vehicle to a race. A vehicle can only be added to a race with status "Pending"
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("add-vehicle")]
        public IHttpActionResult AddVehicleToRace([FromBody]VehicleRaceApi data)
        {
            if (!RaceManager.CheckIfRaceExists(data.RaceId) || RaceManager.GetRaceStatus(data.RaceId) != RaceStatus.Pending)
                return BadRequest("Please select a valid identifier. Modification is only allowed on a pending race.");
            Vehicle vehicle;
            try
            {
                vehicle = new Vehicle((VehicleType)Enum.Parse(typeof(VehicleType), data.VehicleType),
                    data.TeamName, data.VehicleModel, data.ManufacturingDate);
                RaceManager.AddVehicleToRace(vehicle, data.RaceId);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Ok(vehicle.VehicleId);
        }

        /// <summary>
        /// Remove a vehicle from a race. A vehicle can only be removed from a race with status "Pending"
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("remove-vehicle")]
        public IHttpActionResult RemoveVehicleFromRace([FromBody]VehicleRaceIdsApi data)
        {
            if (!RaceManager.CheckIfRaceExists(data.RaceId) || RaceManager.GetRaceStatus(data.RaceId) != RaceStatus.Pending)
                return BadRequest("Please select a valid identifier. Modification is only allowed on a pending race.");
            try
            {
                RaceManager.RemoveVehicleFromRace(data.VehicleId, data.RaceId);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Ok();
        }

        /// <summary>
        /// Start the race. Only a race with status "Pending" can be started
        /// </summary>
        /// <param name="raceId"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("start")]
        public IHttpActionResult StartRace([FromBody]Guid raceId)
        {
            if (!RaceManager.CheckIfRaceExists(raceId) || RaceManager.GetRaceStatus(raceId) != RaceStatus.Pending)
                return BadRequest("Please select a valid identifier. Only a pending race can be started.");
            try
            {
                RaceManager.StartRace(raceId);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Ok();
        }
    }
}
