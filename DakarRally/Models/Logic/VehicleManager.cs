﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DakarRally.Models.Logic
{
    public static class VehicleManager
    {
        public static void UpdateVehicleInfo(Guid raceId, Guid vehicleId, Vehicle rhs)
        {
            Vehicle old = RaceManager.GetVehicleFromRace(raceId, vehicleId);
            old.UpdateVehicle(rhs);
        }
    }
}