﻿using DakarRally.Models.Core;
using DakarRally.Models.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DakarRally.Models
{
    /// <summary>
    /// Race Manager 
    /// </summary>
    public static class RaceManager
    {
        private static List<Race> races = new List<Race>();

        private static Random rng = new Random();

        public static List<Vehicle> GetRaceParticipants(Guid raceId) => races.FirstOrDefault(x => x.RaceIdentifier == raceId).Participants;

        public static bool CheckIfRaceExists(Guid id) => races.Exists(x => x.RaceIdentifier == id);

        public static RaceStatus GetRaceStatus(Guid raceId) => races.FirstOrDefault(x => x.RaceIdentifier == raceId).RaceStatus;

        public static Guid GetRaceForVehicle(Guid vehicleId) => races.FirstOrDefault(x => x.Participants.Exists(y => y.VehicleId == vehicleId)).RaceIdentifier;

        public static Vehicle GetVehicleFromRace(Guid raceId, Guid vehicleId) => races.First(x => x.RaceIdentifier == raceId).Participants.First(y => y.VehicleId == vehicleId);

        public static Guid AddRace(int year)
        {
            Race race = new Race(year);
            races.Add(race);
            return race.RaceIdentifier;
        }

        public static void AddVehicleToRace(Vehicle vehicle, Guid raceId) =>
            races.First(x => x.RaceIdentifier == raceId)?.Participants.Add(vehicle);

        public static void RemoveVehicleFromRace(Guid vehicleId, Guid raceId) =>
            races.First(x => x.RaceIdentifier == raceId)?.Participants.RemoveAll(y => y.VehicleId == vehicleId);

        public static void StartRace(Guid raceId)
        {
            Race race = races.First(x => x.RaceIdentifier == raceId);
            race.UpdateStatus(RaceStatus.Running);

            // For each vehicle in the race
            foreach (Vehicle vehicle in race.Participants)
            {
                VehicleStats stats = new VehicleStats
                {
                    RaceId = raceId,
                    VehicleId = vehicle.VehicleId
                };

                // Calculate the expected drive time
                double eta = Constants.RallyLength / (double)vehicle.MaxSpeed; // t = s/v
                TimeSpan time = TimeSpan.FromHours(eta);
                int hours = time.Days * 24 + time.Hours, minutes = time.Minutes;

                // For each full hour, check if a light / heavy malfunction occured
                int lightCounter = 0; // Count light malfunctions
                for (int i = 0; i < hours; ++i)
                {
                    if (rng.NextDouble() < (double)vehicle.LightMalfunctionProbability / 100) // A light malfunction has occured
                    {
                        ++lightCounter;
                        stats.Malfunctions.Add((MalfunctionType.Light, i * vehicle.MaxSpeed)); // After how many [km] did the malfunction occur
                    }

                    if (rng.NextDouble() < (double)vehicle.HeavyMalfunctionProbability / 100) // A heavy malfunction has occured
                    {
                        stats.Malfunctions.Add((MalfunctionType.Heavy, i * vehicle.MaxSpeed));
                        stats.Finished = false;
                        stats.FinishTime = TimeSpan.MaxValue;
                        stats.Distance = vehicle.MaxSpeed * i; // s = v*t
                        break; // The race is over for this vehicle
                    }
                }
                if (stats.Finished)
                {
                    // For the remaining minutes, check for malfunctions (with scaled probability)
                    double scaledProbLight = ((double)vehicle.LightMalfunctionProbability / 100) * minutes / 60;
                    double scaledProbHeavy = ((double)vehicle.HeavyMalfunctionProbability / 100) * minutes / 60;
                    if (rng.NextDouble() < scaledProbLight) // A light malfunction has occured
                    {
                        ++lightCounter;
                        stats.Malfunctions.Add((MalfunctionType.Light, hours * vehicle.MaxSpeed));
                    }

                    if (rng.NextDouble() < scaledProbHeavy) // A heavy malfunction has occured
                    {
                        stats.Malfunctions.Add((MalfunctionType.Heavy, hours * vehicle.MaxSpeed));
                        stats.Finished = false;
                        stats.FinishTime = TimeSpan.MaxValue;
                        stats.Distance = vehicle.MaxSpeed * hours;
                        continue; // The race is over for this vehicle
                    }

                    stats.Finished = true;

                    // Add the cumulative repairment times for each light malfunction
                    stats.FinishTime = new TimeSpan(hours + lightCounter * vehicle.RepairmentTime, minutes, 0);
                    stats.Distance = Constants.RallyLength;
                }
                StatsManager.Stats.Add(stats);
            }
            race.UpdateStatus(RaceStatus.Finished);
        }

        public static List<(string, int)> GetNumberOfVehiclesByType(Guid raceId)
        {
            Race race = races.First(x => x.RaceIdentifier == raceId);
            List<(string, int)> list = new List<(string, int)>();
            foreach (object x in Enum.GetValues(typeof(VehicleType)))
            {
                list.Add((((VehicleType)x).ToString(), race.Participants.Count(y => y.VehicleType == (VehicleType)x)));
            }
            return list;
        }

        public static int GetNumberOfVehiclesFinished(Guid raceId)
        {
            Race race = races.First(x => x.RaceIdentifier == raceId);
            if (race.RaceStatus != RaceStatus.Finished)
                return 0;
            else
            {
                return StatsManager.Stats.Where(x => x.RaceId == raceId).Count(y => y.Finished);
            }
        }

        public static Vehicle FindVehicle(Guid vehicleId)
        {
            foreach (var x in races)
            {
                var temp = x.Participants.FirstOrDefault(y => y.VehicleId == vehicleId);
                if (temp != null)
                    return temp;
            }
            return null;
        }
    }
}