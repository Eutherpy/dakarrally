﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DakarRally.Models.API
{
    public class VehicleApi
    {
        /// <summary>
        /// Vehicle identifier
        /// </summary>
        [JsonProperty]
        public Guid VehicleId { get; private set; }

        /// <summary>
        /// Vehicle type
        /// </summary>
        [JsonProperty]
        public string VehicleType { get; private set; }

        /// <summary>
        /// Team name
        /// </summary>
        [JsonProperty]
        public string TeamName { get; private set; }

        /// <summary>
        /// Vahicle model name
        /// </summary>
        [JsonProperty]
        public string VehicleModel { get; private set; }

        /// <summary>
        /// Vehicle manufactoring date
        /// </summary>
        [JsonProperty]
        public DateTime ManufacturingDate { get; private set; }
    }
}