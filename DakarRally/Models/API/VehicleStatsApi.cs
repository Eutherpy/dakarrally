﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DakarRally.Models.API
{
    /// <summary>
    /// Vehicle statistics
    /// </summary>
    public class VehicleStatsApi
    {
        /// <summary>
        /// Vehicle type
        /// </summary>
        public string VehicleType { get; private set; }

        /// <summary>
        /// Team name
        /// </summary>
        public string TeamName { get; private set; }

        /// <summary>
        /// Vahicle model name
        /// </summary>
        public string VehicleModel { get; private set; }

        /// <summary>
        /// Vehicle finish time [h]
        /// </summary>
        public TimeSpan FinishTime { get; private set; }

        public VehicleStatsApi(VehicleType vehicleType, string teamName, string vehicleModel, TimeSpan finishTime) =>
            (VehicleType, TeamName, VehicleModel, FinishTime) = (vehicleType.ToString(), teamName, vehicleModel, finishTime);
    }
}