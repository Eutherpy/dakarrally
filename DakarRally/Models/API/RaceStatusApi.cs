﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DakarRally.Models.API
{
    /// <summary>
    /// Detailed race status
    /// </summary>
    public class RaceStatusApi
    {
        /// <summary>
        /// Race status
        /// </summary>
        public string RaceStatus { get; set; }

        /// <summary>
        /// Number of participants grouped by vehicle type
        /// </summary>
        public List<(string, int)> ParticipantsByType = new List<(string, int)>();

        /// <summary>
        /// Number of participants who finished the race
        /// </summary>
        public int NumberOfParticipantsFinished { get; set; }
    }
}