﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DakarRally.Models.API
{
    /// <summary>
    /// Identifier of a vehicle in a race
    /// </summary>
    public class VehicleRaceIdsApi
    {
        /// <summary>
        /// Vehicle identifier
        /// </summary>
        [JsonProperty]
        public Guid VehicleId { get; private set; }

        /// <summary>
        /// Race identifier
        /// </summary>
        [JsonProperty]
        public Guid RaceId { get; private set; }
    }
}