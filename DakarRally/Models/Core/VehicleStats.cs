﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DakarRally.Models.Core
{
    /// <summary>
    /// Statistics of a vehicle participating in a race
    /// </summary>
    public class VehicleStats
    {
        /// <summary>
        /// Vehicle identifier
        /// </summary>
        [JsonIgnore]
        public Guid VehicleId { get; set; }

        /// <summary>
        /// Race identifier
        /// </summary>
        [JsonIgnore]
        public Guid RaceId { get; set; }

        /// <summary>
        /// Vehicle finish status
        /// </summary>
        [JsonProperty]
        public bool Finished { get; set; }

        /// <summary>
        /// Vehicle finish time [h]
        /// </summary>
        [JsonProperty]
        public TimeSpan FinishTime { get; set; }

        /// <summary>
        /// Distance driven [km]
        /// </summary>
        [JsonProperty]
        public double Distance { get; set; }

        /// <summary>
        /// List of malfunctions and the distances where they occured
        /// </summary>
        [JsonProperty]
        public List<(MalfunctionType, int)> Malfunctions = new List<(MalfunctionType, int)>();
    }
}