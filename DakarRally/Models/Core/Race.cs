﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DakarRally.Models
{
    /// <summary>
    /// Race
    /// </summary>
    public class Race
    {
        /// <summary>
        /// Race indentifier
        /// </summary>
        public Guid RaceIdentifier { get; private set; }

        /// <summary>
        /// Year
        /// </summary>
        public int Year { get; private set; }

        /// <summary>
        /// Race status
        /// </summary>
        public RaceStatus RaceStatus { get; private set; }

        /// <summary>
        /// List of participants in the race
        /// </summary>
        public List<Vehicle> Participants { get; private set; }

        public Race(int year) =>
            (RaceIdentifier, Year, Participants, RaceStatus) = (Guid.NewGuid(), year, new List<Vehicle>(), RaceStatus.Pending);

        public void UpdateStatus(RaceStatus newStatus) => RaceStatus = newStatus;
    }
}