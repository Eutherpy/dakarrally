﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DakarRally.Models
{
    public static class Constants
    {
        public const int RallyLength = 10000;

        public static Dictionary<VehicleType, int> MaxSpeed = new Dictionary<VehicleType, int>()
        {
            {VehicleType.CrossMotorcycle, 85 },
            {VehicleType.SportMotorcycle, 130 },
            {VehicleType.SportsCar, 140 },
            {VehicleType.TerrainCar, 100 },
            {VehicleType.Truck, 80 }
        };

        public static Dictionary<VehicleType, int> RepairTimes = new Dictionary<VehicleType, int>()
        {
            {VehicleType.CrossMotorcycle, 3 },
            {VehicleType.SportMotorcycle, 3 },
            {VehicleType.SportsCar, 5 },
            {VehicleType.TerrainCar, 5 },
            {VehicleType.Truck, 7 }
        };

        public static Dictionary<VehicleType, int> LightMalfunctionProbability = new Dictionary<VehicleType, int>()
        {
            {VehicleType.CrossMotorcycle, 3 },
            {VehicleType.SportMotorcycle, 18 },
            {VehicleType.SportsCar, 12 },
            {VehicleType.TerrainCar, 3 },
            {VehicleType.Truck, 6 }
        };

        public static Dictionary<VehicleType, int> HeavyMalfunctionProbability = new Dictionary<VehicleType, int>()
        {
            {VehicleType.CrossMotorcycle, 2 },
            {VehicleType.SportMotorcycle, 10 },
            {VehicleType.SportsCar, 2 },
            {VehicleType.TerrainCar, 1 },
            {VehicleType.Truck, 4 }
        };
    }
}