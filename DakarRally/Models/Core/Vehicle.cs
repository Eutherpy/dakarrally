﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DakarRally.Models
{
    /// <summary>
    /// Vahicle
    /// </summary>
    public class Vehicle
    {
        /// <summary>
        /// Vehicle identifier
        /// </summary>
        public Guid VehicleId { get; private set; }

        /// <summary>
        /// Vehicle type
        /// </summary>
        public VehicleType VehicleType { get; private set; }

        /// <summary>
        /// Team name
        /// </summary>
        public string TeamName { get; private set; }

        /// <summary>
        /// Vahicle model name
        /// </summary>
        public string VehicleModel { get; private set; }

        /// <summary>
        /// Vehicle manufactoring date
        /// </summary>
        public DateTime ManufacturingDate { get; private set; }

        /// <summary>
        /// Vehicle max speed [km/h]
        /// </summary>
        public int MaxSpeed { get; private set; }

        /// <summary>
        /// Vehicle repairment time (in case of light malfunction) [h]
        /// </summary>
        public int RepairmentTime { get; private set; }

        /// <summary>
        /// Probability of light malfunction [percentage/hour]
        /// </summary>
        public int LightMalfunctionProbability { get; private set; }

        /// <summary>
        /// Probability of heavy malfunction [percentage/hour]
        /// </summary>
        public int HeavyMalfunctionProbability { get; private set; }

        /// <summary>
        /// Vehicle constructor
        /// </summary>
        /// <param name="vehicleType"></param>
        /// <param name="teamName"></param>
        /// <param name="vehicleModel"></param>
        /// <param name="manufacturingDate"></param>
        public Vehicle(VehicleType vehicleType, string teamName, string vehicleModel, DateTime manufacturingDate)
        {
            VehicleId = Guid.NewGuid();
            VehicleType = vehicleType;
            TeamName = teamName;
            VehicleModel = vehicleModel;
            ManufacturingDate = manufacturingDate;
            MaxSpeed = Constants.MaxSpeed[vehicleType];
            RepairmentTime = Constants.RepairTimes[vehicleType];
            LightMalfunctionProbability = Constants.LightMalfunctionProbability[vehicleType];
            HeavyMalfunctionProbability = Constants.HeavyMalfunctionProbability[vehicleType];
        }

        public void UpdateVehicle(Vehicle newInfo)
        {
            VehicleType = newInfo.VehicleType;
            VehicleModel = newInfo.VehicleModel;
            TeamName = newInfo.TeamName;
            ManufacturingDate = newInfo.ManufacturingDate;
        }
    }
}