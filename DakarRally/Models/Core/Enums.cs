﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DakarRally.Models
{
    public enum VehicleType
    {
        SportsCar,
        TerrainCar,
        Truck,
        CrossMotorcycle,
        SportMotorcycle
    }

    public enum RaceStatus
    {
        Pending,
        Running,
        Finished
    }

    public enum MalfunctionType
    {
        Light,
        Heavy
    }
}