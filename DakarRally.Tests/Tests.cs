﻿using System;
using System.Web.Http;
using System.Web.Http.Results;
using DakarRally.Controllers;
using DakarRally.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DakarRally.Tests
{
    [TestClass]
    public class Tests
    {
        [TestMethod]
        public void CreateRaceInvalidYear()
        {
            RaceController rc = new RaceController();
            IHttpActionResult result = rc.CreateRace(1002);
            Assert.AreEqual(result.GetType(), typeof(BadRequestErrorMessageResult));
        }

        [TestMethod]
        public void AddInvalidVehicle()
        {
            RaceController rc = new RaceController();
            IHttpActionResult result = rc.AddVehicleToRace(new Models.API.VehicleRaceApi());
            Assert.AreEqual(result.GetType(), typeof(BadRequestErrorMessageResult));
        }

        [TestMethod]
        public void StartNonexistingRace()
        {
            RaceController rc = new RaceController();
            IHttpActionResult result = rc.StartRace(new Guid()); // Empty guid
            Assert.AreEqual(result.GetType(), typeof(BadRequestErrorMessageResult));
        }

        [TestMethod]
        public void GetRaceParticipants()
        {
            Guid raceId = RaceManager.AddRace(2000);
            Assert.AreEqual(0, RaceManager.GetRaceParticipants(raceId).Count);
        }

        [TestMethod]
        public void StartAlreadyFinishedRace()
        {
            Guid raceId = RaceManager.AddRace(2000);
            RaceManager.StartRace(raceId);
            try
            {
                RaceManager.StartRace(raceId);
                Assert.Fail();
            }
            catch (Exception) { }
        }
    }
}
